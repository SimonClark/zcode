<?php
	require('site.conf.php');
	require('zcode/ZCode.php');

	if (!ZI18n::$on || !ZI18n::$authorMode) {
		die;
	}
?>
<div class="i18n-clickshield">
	<div class="i18n-authoring-form">
		<h3>Enter Translation (<?=ZI18n::GetLang(); ?>):</h3>
		<div>
			<b>Translation Key: </b><br />
			<input name="key" disabled="disabled" value="<?=$_POST['key'];?>" />
		</div>
		<?php if (!empty($_POST['insertKeys'])) {
			?>
				<div>
					<b>Value Insertion Keys: </b><br />
					<?=$_POST['insertKeys'];?>
				</div>
			<?php
		} ?>
		<div>
			<b>Translation: </b><br />
			<textarea id="i18n-translation" name="translation"><?=ZI18n::GetRawPhrase($_POST['key']);?></textarea>
		</div>
		<div>
			<button onclick="zCode_saveTranslation();">Save Translation</button>
			<button onclick="$('.i18n-clickshield').remove();">cancel</button>
		</div>
	</div>
</div>