<?php
    global $checkLang;
    $enTranslations = ZI18n::loadTranslations(ZI18n::$defaultLang);
    ZI18n::SetLang($checkLang);
    $checkTranslations = ZI18n::loadTranslations($checkLang);
    setcookie('lang', $checkLang, 0, '/');
    ?>
    Shift click on a translation key, or translated phrase to edit/enter a translation.<br />
    Refresh the page to see entered translations show up<br />
    &nbsp;
    <table>
        <tr><th>Key</th><th>English</th><th><?=$checkLang;?></th></tr>
        <?php foreach ($enTranslations as $translationKey => $enTranslation) { ?>
            <?php 
                $error = !array_key_exists($translationKey, $checkTranslations);
                $phrase = $error ? $translationKey : $checkTranslations[$translationKey];
            ?>
            <tr class="<?= $error ? "error" : '';?>">
            <td><span class="i18n-phrase small" data-key="<?=$translationKey;?>"><?=$translationKey;?></span></td>
            <td><?=$enTranslation;?></td>
            <td><span class="i18n-phrase <?= $error ? "i18n-error" : '';?>" data-key="<?=$translationKey;?>"><?=$phrase;?></span></td>
            </tr>
        <?php } ?>
    </table>