$().ready((e) => {
	zCode_BindAuthoring();
})

function zCode_BindAuthoring() {
	$('.i18n-phrase').click((event) => {
		var clickshield_mousedown = false;
		if (event.shiftKey) {
			event.stopPropagation();
			$.ajax({ type: "POST",   
				 url: zcode.path + "i18n/authoringForm.php",
				 data: {
					key: event.currentTarget.dataset.key,
					insertKeys: event.currentTarget.dataset.insertkeys
				 },
				 success : function(text)
				 {
					 $('body').append(text);
					 $('.i18n-authoring-form').mousedown((e) => {e.stopPropagation();}).mouseup((e) => {e.stopPropagation();});
					 $('.i18n-clickshield').mousedown((event)=>{
						clickshield_mousedown = true;
						setTimeout(()=>{clickshield_mousedown = false}, 1000);
					 }).mouseup((event) => {
						clickshield_mousedown && $('.i18n-clickshield').remove();
					 });
					 $('#i18n-translation').focus();
				 }
			});
			return false;
		}
	}).mouseup((event) => {
		if (event.shiftKey) {
			event.stopPropagation();
			return false;
		}
	});
}

function zCode_saveTranslation() {
	var params = {lang: zcode.lang};
	$('.i18n-authoring-form').find('input, textarea').each((i, element) => {
		params[element.name] = $(element).val();
	});

	$.ajax({ type: "POST",   
	     url: zcode.path + "i18n/saveTranslation.php",
	     data: params,
	     success : function(text)
	     {
		 	$('.i18n-clickshield').remove();
	     }
	});
}

function i18n(translationKey, inserts) {
	let translation = zcode.i18n[translationKey];
	if (translation) {
		if (inserts) {
			for (let key in inserts) {
				translation = translation.replace('{$' + key + '}', inserts[key]);
			}
		}
		return translation;
	}
	let untranslatedPlaceholder = '<span class="i18n-error" data-keys="' + JSON.stringify(Object.keys($inserts)) + '" >' + translationKey + '</span>';
	ZLogI18nError(untranslatedPlaceholder);
	return untranslatedPlaceholder;
}