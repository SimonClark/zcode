1.3.0 : Add ZAuth::CheckPassword() for simple authentication

1.2.2 : Z18n updated authoring mode so that it can work in dynamically loaded content.  Need to call zCode_bindAuthoring() after load success

1.2.1 : Changing zUtils::LinkMailIfMember() to LinkMailIfLoggedIn()
