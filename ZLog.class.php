<?php
	class ZLog {
		private static $debuggerData = array('parse' => array(), 'i18n' => array());
		private static $lasttimes = array();
		public static $errors = array();
		private static $tags = array('i18n' => array());

		public static function Save($comment) {
			ZDB::Insert('logs', array(
				'ip' => $_SERVER['REMOTE_ADDR'],
				'timestamp' => time(),
				'comment' => $comment
			));
		}
		
		// Debugger stuff
		public static function Log($name, $message, $tab='parse', $saveToFile = false) {
			if ($tab == 'error') {
				ZLog::$errors[] = $name . ' -> ' . $message;
			}
			if (is_array($message) || is_object($message)) {
				$message = print_r($message, TRUE);
			}
			if ($saveToFile) {
				ZLog::saveToFile($message);
			}
			$tab = strtolower($tab);
			ZLog::$debuggerData[$tab][] = array(
				'name' => $name,
				'msg' => $message
			);
		}
	
		public static function Error($name, $message, $tab='parse', $saveToFile = false) {
			ZLog::$errors[] = $name . ' -> ' . print_r($message, true);
			ZLog::log($name, $message, $tab, $saveToFile);
		}

		public static function Tag($span, $tab='parse') {
			if (!array_key_exists($tab, self::$tags)) {
				self::$tags[$tab] = array();	
			}
			self::$tags[$tab][] = $span;
		}
	
		public static function ErrorIfEmpty($name, $value) {
			if (empty($value)) {
				ZLog::error($name . ' cannot be empty!', ZLog::FormatStackTrace(debug_backtrace()));
			}
		}
	
		public static function LogEmail($address, $title, $message, $headers) {
			global $beta;
			$json = json_encode(array(
				'to' => $address,
				'title' => $title,
				'message' => $message,
				'headers' => $headers
			));
			ZLog::log('Email Sent', $json, 'email');
			if ($beta) {
				DiyodeSite::SetSetting('_last_email_sent', $json);
			}
		}


		public static function FormatStackTrace($stackTrace) {
			$out = "\n<div class=\"stack-trace\">\n";
			foreach ($stackTrace as $stack) {
				$out .= "\t" . $stack['file'] . ', Line ' . $stack['line'];
				$out .= ' - ' .$stack['class'].$stack['type'].$stack['function'] . '(';
				$out .= implode(', ', array_map('ZLog::ParamToString', $stack['args']));
				$out .= ")      <br />\n";
			}
			$out .= "</div>\n\n";
// 			$out .= print_r($stackTrace, true);
			return $out;
		}
		
		public static function ParamToString($param) {
			if ($param == null) {
				return 'null';
			}
			$type = gettype($param);
			if ($type == 'string') {
				return '"'.$param.'"';
			}	
			if ($type == 'integer' || $type == 'double') {
				return strval($param);
			}	
			return '['.$type.']';	
		}
		
		public static function pr() {
			print_r(ZLog::$debuggerData);
		}
		
		public static function Print() {
			if (ZCode::$debug) {
				if (ZRequest::ShouldRenderAsHTML()) {
					self::PrintHTML();
				} else {
					self::PrintPlainText();
				}
			}
		}

		public static function PrintHTML() {
			ZRequest::PrintJQuery();
			?>
				<script type="text/javascript">
					function ZLog(name, message) {
						jQuery('#logTabjavascript').append('<div class="logTabLine"><h4>'+name+'</h4>'+message+'</div>');
					}
					function ZLogTab(el, tabName) {
						$('.logTab').hide();$('#logTab'+tabName).show();
						$('.logHead a.picked').removeClass('picked');
						$(el).addClass('picked');
						window.scrollBy(0, 100);
					}
					function ZLogTab(el, tabName) {
						$('.logTab').hide();$('#logTab'+tabName).show();
						$('.logHead a.picked').removeClass('picked');
						$(el).addClass('picked');
						window.scrollBy(0, 100);
					}
					function ZLogShowRemains(el) {
						$(el).parent().find('.log-more-span').toggle();
					}
					function ZLogI18nError(untranslatedPlaceholder) {

					}

				</script>
			<style>
				.logHead {margin: 11px 14px 0px;}
				.logHead a {padding: 3px 5px 0px 9px; background: #bbb; border: 1px #444 solid; color: #fff; font-size: 16px; text-decoration: none; border-bottom: none;}
				.logHead a.picked {padding: 3px 5px 1px 9px; color: 000;}
				.logTab {background: #bbb; padding: 5px; border: 1px solid black; display: none;margin: 0px 6px 10px;}
				.logTab .logTabLine { margin: 2px; border: 1px #888 solid; padding: 2px; font-size: 10px;  line-height: 1.1em;}
				.logTab .logTabLine h4 { padding: 0px; margin: 0 0 3px 0; font-size: 1.1em; }
				.log-more-span {display: none;}
				.logTab .tags span {margin: 0px;line-height: 22px;padding: 0px 4px;background-color: #bfffbb8c;}
			</style>
				
			<div style="clear:both; margin:5px 0 0 0;">
				<a href="#" style="opacity: 0.5;font-size: 0.8em;margin: 5px 10px 0px;" onclick="$('#logDiv').toggle(); window.scrollBy(0, 100); return false;">Show/Hide Logger</a>
				<div id="logDiv" style="display:none;">
					<div class="logHead">
						<?php
							ZLog::$debuggerData['javascript'] = array();
							foreach (ZLog::$debuggerData as $key => $data) {
								?><a href="#" onclick="ZLogTab(this, '<?=$key?>'); return false;">
								<?php echo $key?> </a> &nbsp; <?php
							}
						?>
					</div>
					<?php foreach (ZLog::$debuggerData as $key => $data) { ?>
						<div id="logTab<?php echo $key?>" class="logTab">
							<div class="tags">
								<?php if (!empty(self::$tags[$key])) { 
									echo implode(' ', self::$tags[$key]);
								} ?>
							</div>
							<?php
							foreach ($data as $line) {
								?>
								<div class="logTabLine">
									<h4><?php echo $line['name']?></h4>
									<pre><?php 
										echo h(substr($line['msg'],0,120));
										$remains = h(substr($line['msg'],120));
										if (!empty($remains)) {
											echo '<span class="log-more-span">'.h(substr($line['msg'],120)).'</span>';
											echo ' <a href="#" onclick="ZLogShowRemains(this); return false;">more...</a>';
										}
									?></pre>
								</div>
								<?php
							}
							?>	
						</div>
					<?php } ?>
				</div>
			</div>
			<?php
		}

		public static function PrintPlainText() {
			foreach (ZLog::$debuggerData as $key => $data) {
				echo "\n\n------------  " . $key . "  ------------\n";
				foreach ($data as $line) {
					echo '-- ' . $line['name'] . ": \n";
					echo $line['msg'] . "\n\n";
				}
			}
		}
		
		public static function SaveToFile($msg) {
			file_put_contents(ZCode::$path . '../logs/site.log', $msg . "\r\n", FILE_APPEND);
		}
	
		public static function ts($message = "Boo") {
			$time = microtime();
			if (ZLog::$lasttimes[$message]) {
				echo($message . " -> " . ($time - ZLog::$lasttimes[$message]) . "<br />");
			}
			ZLog::$lasttimes[$message] = $time;
		}
	}

	ZLog::log('Get', $_GET);
	ZLog::log('Post', $_POST);
/* 	ZLog::log('Session', $_SESSION); */
	ZLog::log('Cookie', $_COOKIE);
	ZLog::log('Server', $_SERVER);

