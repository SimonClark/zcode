<?php
	// -----------------------------------------------------------------------------------------------------------
	// -----------------------------------   Database interaction code   -----------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	
	class ZDB {
		private static $error = false;
		private static $errors = array();
		private static $links = array();

		private static $dbs;
		private static $currentDB = 'primary';
		
		public static function Init() {
			global $ZCodeConf;
			self::$dbs = $ZCodeConf['dbs'];
		}
		
		public static function SetDB($dbName) {
			self::$currentDB = $dbName;
		}

		public static function ResetDB() {
			self::$currentDB = 'primary';
		}

		public static function Connect() {
			if (!empty(self::$links[self::$currentDB])) {
				return self::$links[self::$currentDB];
			}
			$db = self::$dbs[self::$currentDB];
			self::$links[self::$currentDB] = new PDO('mysql:host='.$db['host'].';dbname='.$db['db'], $db['user'], $db['password']);	
		 	self::$links[self::$currentDB]->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			return self::$links[self::$currentDB];
		}
	
		public static function Disconnect() {
			self::$links = array();
		}
	
		public static function GetQuery($query, $params = array(), $options = array()) {
			global $verbose;
			self::$error = false;
			
			$list = array();
			if (isset($query)) {
				$grLink = self::Connect();
				ZLog::log('GetQuery: ' . $query, $params,  'SQL', true);
				self::pr( $params, $query);
				try {
					$statement = $grLink->prepare($query);
					$success = $statement->execute($params);
				} catch( PDOException $Exception ) {
					self::$errors[] = self::$error = $Exception->getMessage( );
					ZLog::Error('PDO Query Error!', self::$error, 'SQL', true);
					self::pr(self::$error);
					return false;
				}
	
				try {
					if ($success) {
						while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
							if (isset($row['index'])) {
								$list[$row['index']] = $row;
							} else {
								$list[] = $row;
							}
						}
					}
				} catch( PDOException $Exception ) {
					$error = $Exception->getMessage();
					if ($error && strpos($error, 'General error') === false) {
						self::$error = $error;
						ZLog::Error('PDO Result Error!', self::$error, 'SQL', true);
						self::$errors[] = 'SQL Error: ' . $error;
					}
					return true;
				}
			}
			ZLog::log('QueryResult', $list, 'SQL');
			if (!empty($options['keyedBy'])) {
				$list = self::SortRecords($list, $options['keyedBy'], $options['keyPrepend']);
			}
			return $list;
		}

		public static function pr($out, $label=false) {
			global $verbose;
			if ($verbose) {
				return ZUtils::pr($out, $label);
			}
		}

		public static function GetRecord($table, $field, $value=0, $append='') {
			$results = self::GetRecords($table, $field, $value, $append . ' LIMIT 0,1');
			self::pr($results, 'GetRecord unfiltered Result');
			if (!empty($results)) {
				return $results[0];
			}
			return array();
		}
		
		public static function GetRecords($table, $field, $value = 0, $append = '') {
			if (is_array($field)) {
				$query = "SELECT * FROM $table WHERE 1 ";
				$params = array();
				foreach ($field as $key => $value) {
					$query .= ' AND `'.$key.'` = ?';
					$params[] = $value;
				}
				$query .= $append;
				$result = self::GetQuery($query, $params);
				return $result;
			}
			$query = "SELECT * FROM $table WHERE `status`> -1 AND `$field` LIKE ? " . $append;
			$result = self::GetQuery($query, array($value));
			return $result;
		}
	
		public static function InsertOrUpdate($table, $fields) {
			if (!empty($fields['id'])) {
				return self::Update($table, $fields);
			} else {
				unset($fields['id']);
				return self::Insert($table, $fields);
			}
		}
	
		public static function Insert($table, $fields) {
			$sql = 'INSERT INTO ' . $table . ' (`'.implode('`, `', array_keys($fields)).'`) VALUES ('.self::QuestionMarks(count($fields)).');';
			$params = array_values($fields);
			self::GetQuery($sql, $params);
			$records = 	self::GetQuery('SELECT * FROM ' . $table . ' WHERE id = ? LIMIT 1;', array(self::GetLastInsertId()));
			return $records[0];
		}
	
		public static function Update($table, $fields) {
			$id = $fields['id'];
			$fieldSets = array();
			$params = array();
			foreach ($fields as $fieldName => $fieldValue) {
				if ($fieldName != 'id') {
					$fieldSets[] = '`'.$fieldName.'` = ?';
					$params[] = $fieldValue;
				}
			}
			$sql = 'UPDATE '.$table.' SET '.implode(', ', $fieldSets) . ' WHERE id = ?;';
			$params[] = $fields['id'];
			self::GetQuery($sql, $params);
			return self::GetRecord($table, 'id', $id);
		}
	
		public static function CountRecords($table, $field, $value) {
			if (isset($value)) {
			    $row = self::GetQuery(
			    	"SELECT count(*) as count FROM $table WHERE `status`> -1 AND $field LIKE ? ",
			    	array($value)
			    );
			}
			return $row[0]['count'];
		}
		
		public static function GetLastInsertId() {
			$grLink = self::Connect();
			$id = $grLink->lastInsertId();
			return $id;
		}
		
		public static function Delete($table, $field, $value) {
			self::GetQuery(
				"DELETE FROM ? WHERE ? = ? LIMIT 1",
				array($table, $field, $value)
			);
		}
		
		public static function SortRecords($list, $keyId, $prepend=null) {
			$out = array();
			foreach ($list as $rec) {
				$key = $prepend == null ? $rec[$keyId] : strval($prepend . $rec[$keyId]);
				$out[$key] = $rec;
			}
			return $out;
		}
			
		public static function DiffToString($record1, $record2) {
			$out = '';
			foreach ($record1 as $key => $value) {
				if ($record1[$key] != $record2[$key]) {
					$value2 = $record2[$key];
					$out .= "$key changed from '$value' to '$value2', ";
				}
			}
			if (empty($out)) {
				return false;
			}
			return substr($out, 0, -2);
		}
		
		public static function RecordHash($record, $fields) {
			return md5('fsdgdgsdgertwew45674ufgd' . json_encode($record));
		}
			
		public static function Safe($value, $filter=null) {
			if ($filter == 'int')
				return intval($value);
			else if ($filter == 'float')
				return floatval($value);
			else if ($filter == 'email')
				return preg_replace('/[^a-zA-Z0-9*._-+]/', '', $value);
			else if ($filter == 'alphanum')
				return preg_replace('/[^a-zA-Z0-9_]/', '', $value);
			else if ($filter == 'alphanumplus')
				return preg_replace('/[^a-zA-Z0-9_ ]/', '', $value);
			else 
				return addslashes($value);
		}
	
		public static function QuestionMarks($num) {
			if (is_array($num)) {
				$num = count($num);
			}
			$questionmarks = '';
			for ($i=0; $i<$num; $i++) {
			    $questionmarks .= "?,";
			}
			$questionmarks = trim($questionmarks, ",");
			return $questionmarks;
		}
		
		public static function DieIfError() {
			if (self::HasError()) {
				ZRequest::Error('There were SQL Errors',self::$errors);
			}
		}

		public static function HasError() {
			return !empty(self::$errors);
		}
	}
	