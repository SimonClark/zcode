# README #

Common ZCode framework

### What is this repository for? ###

* Initially developed for villagetoolbox.com - providing a rapid build solution for Simon & Dahlia Clark
* Version 1.2

Sample Config Object: 

	$SiteDomain = 'beta.site.com';

	$ZCodeConf = array(
		'site' => array(
			'beta' => true,
			'debug' => true,
			'sessionName' => 'rmsite',
			'path' => '/var/www/'.$SiteDomain.'/html/',
			'domain' => $SiteDomain,
			'siteName' => $SiteDomain,
			'root' => 'https://'.$SiteDomain.'/'
		),
		'auth' => array(
			'userTable' => 'users',
			'salt' => 'fgsssdfddghp3094tr567dt3fsddf',
			'adminEmail' => 'simon@zebraspot.com',
			'authFolder' => 'https://'.$SiteDomain.'/',
		),
		'dbs' => array(
			'primary' => array(
				'host' => 'localhost',
				'user' => 'dbuser',
				'password' => 'dbpassword',
				'db' => 'sitedotcom'
			),
			'villagetoolbox' => array(
				'host' => 'localhost',
				'user' => 'dbuser2',
				'password' => 'dbpassword2',
				'db' => 'seconddb'
			)
		),
		'request' => array(
			'header' => 'includes/header.php',
			'footer' => 'includes/footer.php'
		)
	);