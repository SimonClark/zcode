<?php
	// -----------------------------------------------------------------------------------------------------------
	// -----------------------------------   Internationalization code   -----------------------------------------
	// -----------------------------------------------------------------------------------------------------------
	
	class ZI18n extends ZBase {
		public static $defaultLang = 'en_ca';
		protected static $availableLangs = array('en_ca' => 'English');
		private static $currentLang = 'en_ca';
		protected static $fileLocation = null;
		private static $translations = array();
		public static $on = false;
		public static $authorMode = false;
		public static $wrapInPTags = false;
		
		public static function Init() {
			self::Config('i18n');
			ZLog::Log('Translation File Location', ZCode::$path . self::$fileLocation, 'i18n');

			if (!empty($_GET['lang']) && !empty(self::$availableLangs[$_GET['lang']])) {
				self::$currentLang = $_GET['lang'];
				setcookie('lang', self::$currentLang, time() + (365 * 24 * 3600), '/');
			} else if (!empty($_COOKIE['lang']) && !empty(self::$availableLangs[$_COOKIE['lang']])) {
				self::$currentLang = $_COOKIE['lang'];
			} else {
				self::$currentLang = self::$defaultLang;
			}
		}
		
		public static function PrintI18nAuthoring() {
			if (self::$on) {
				?>
					<link rel="stylesheet" href="<?=ZCode::$zcodePath;?>/i18n/css/i18n.css">
					<script src="<?=ZCode::$zcodePath;?>/i18n/js/i18n.js"></script>
				<?php				
				if (self::$authorMode) {
				}
			}
		}

		public static function GetLang() {
			return self::$currentLang;
		}
		public static function SetLang($lang) {
			self::$currentLang = $lang;
		}
		
		public static function GetLanguageSelector() {
			$out .= '<ul class="lang-selector">';
			$uri = $_SERVER['REQUEST_URI'];
			$uri .= ((strpos($uri, '?') === false) ? '?' : '&') . 'lang=';
			foreach (self::$availableLangs as $lang => $langName) {
				$out .= '<li><a href="' . $uri . $lang . '">' . $langName . '</a></li>';
			}
			$out .= '</ul>';
			return $out;
		}
		
		private static function LoadSiteTranslations() {
			if (empty(self::$translations)) {
				self::$translations = self::LoadTranslations();
			}
		}

		public static function LoadTranslations($lang = false) {
			//self::LoadTranslationFile(__DIR__ . '/i18n/lang/' . self::$defaultLang . '.json');
			if (!$lang) {
				$lang = self::$currentLang;
			}
			$translations = array();
			$translations = self::LoadTranslationFile(__DIR__ . '/i18n/lang/' . $lang . '.json', $translations);
			if (self::$on) {
				$translations = self::LoadTranslationFile(ZCode::$path . self::$fileLocation . $lang . '.json', $translations);
			}
			return $translations;
		}

		public static function GetTranslationFileURL() {
			return ZCode::$path . self::$fileLocation . self::$currentLang . '.json';
		}
		
		private static function LoadTranslationFile($filePath, &$translations) {
			if (file_exists($filePath)) {
				$contents = file_get_contents($filePath);
				$phraseArray = json_decode($contents, true);
				if ($phraseArray === null || gettype($phraseArray) != 'array') {
					var_dump($phraseArray);
					pr (gettype($phraseArray),'phraseArray');
					die('Translation Decode Error '. json_last_error_msg() . ': ' . $filePath);
					ZRequest::Error('Translation Decode Error', json_last_error_msg() . ': ' . $filePath);
				}
				$translations = array_merge($translations, $phraseArray);
				ZLog::Log('Translation File Loaded', $filePath, 'i18n');
			} else {
				ZLog::Log('Translation File Not Found!', $filePath, 'i18n');
			}
			return $translations;
		}

		public static function GetRawPhrase($translationKey, $inserts = null, $wrap = false) {
			self::LoadSiteTranslations();
			if (array_key_exists($translationKey, self::$translations)) {
				$phrase = self::$translations[$translationKey];
				$notFound = false;
				if (!empty($inserts)) {
					foreach ($inserts as $insertKey => $insert) {
						$phrase = str_replace('{$'.$insertKey.'}', $insert, $phrase);
					}
				}
				if (self::$wrapInPTags) {
					$phrase = '<p>' . str_replace(array("\r\n", "\r", "\n"), '</p><p>', $phrase) . '</p>';
				}
			} else {
				$notFound = true;
				$phrase = $translationKey;
			}
			self::$wrapInPTags = false;
			if (self::$authorMode) {
				ZLog::Tag(self::WrapTranslation($translationKey, $translationKey, $inserts, $notFound), 'i18n');
				if ($wrap) {
					return self::WrapTranslation($phrase, $translationKey, $inserts, $notFound);
				} else {
					return $phrase;
				}
			}
			return $phrase;
		}
		
		public static function WrapTranslation($phrase, $translationKey, $inserts, $error) {
			$dataInsertKeys = (!empty($inserts)) ? (' data-insertkeys="' . implode(',', array_keys($inserts)) . '"') : '';
			$dataKey = ' data-key="' . $translationKey . '"';
			return '<span class="i18n-phrase'.($error ? ' i18n-error':'').'" ' . $dataKey .  $dataInsertKeys . '>' . $phrase . '</span>';
		}

		public static function GetPhrase($key, $inserts = null) {
			return self::GetRawPhrase($key, $inserts, true);
		}

		public static function TranslateArray($arr) {
			if (empty($arr)) return array();
			$result = array();
			foreach ($arr as $key => $value) {
				$result[$key] = self::GetRawPhrase($value);
			}
			return $result;
		}
		
		public static function GetPhraseWithPTag($key, $inserts = null) {
			self::$wrapInPTags = true;
			return self::GetPhrase($key, $inserts);
		}
		
		public static function GetJSTranslations() {
			$jsTranslations = array();
			if (self::$on) {
				self::LoadSiteTranslations();
				foreach (self::$translations as $key => $translation) {
					if ($key[0] == '_') {
						$jsTranslations[$key] = $translation;
					}
				}
			}
			return $jsTranslations;
		}
		
	}
	
	function i18n($key, $inserts = null) {
		return ZI18n::GetPhrase($key, $inserts);
	}
	
	function i18nRaw($key, $inserts = null) {
		return ZI18n::GetRawPhrase($key, $inserts);
	}
	
	function i18nPTag($key, $inserts = null) {
		return ZI18n::GetPhraseWithPTag($key, $inserts);
	}