<?php

	class ZUtils extends ZBase {
		public static $curlCookies = array();
		public static $reCaptchaPublicKey = '';
		public static $reCaptchaSecretKey = '';
		public static $reCaptchaURL = '';
				
		public static function Init() {
			self::Config('utils');	
		}
		
		public static function filterShortName($val) {
			return preg_replace('/[^abcdefghijklmnopqrstuvwxyz123456789]/','',strtolower($val));
		}
	
		public static function GetRawValue($name, $default=null, $filter='any') {
			if (isset($_GET[$name]))
				$v = $_GET[$name];
			else if (isset($_POST[$name]))
				$v = $_POST[$name];
			else if (isset($_COOKIE[$name]))
				$v = $_COOKIE[$name];
			else if (isset($_SESSION[$name]))
				$v = $_SESSION[$name];
			else
				$v = $default;
			return $v;
		}
	
		public static function GetRawPost($name, $default=null, $filter='any') {
			if (isset($_POST[$name]))
				$v = $_POST[$name];
			else
				$v = $default;
			return $v;
		}
	
		public static function GetValue($name, $default=null, $filter='any') {
			return ZDB::Safe(ZUtils::GetRawValue($name, $default, $filter), $filter);
		}
	
		public static function GetPost($name, $default=null, $filter='any') {
			$v = ZDB::Safe(ZUtils::GetRawValue($name, $default, $filter), $filter);
			if (($v=='' || $v==NULL) && $filter != 'any') {
				throw new Exception("Value '$name' is empty");
			}
	
			return $v;
		}
	
		public static function GetStackTrace($fullArgs = false) {
			$stack = debug_backtrace();
			$out = array();
			foreach ($stack as $i => $method) {
				if ($i > 0) {
					$line = $method['file'] . ', Line ' . $method['line'] . ' -> ' . $method['class'].$method['type'].$method['function'] . '(';
					$line .= self::PrintArgs($method['args'], $fullArgs);
					$line .= ');';
					$out[] = $line;
				}
			}
			return $out;	
		}
		
		private static function PrintArgs($args, $fullArgs) {
			$out = array();	
			foreach ($args as $arg) {
				if ($fullArgsx) {
					$out[] = self::PrintArg($arg);
				} else if (is_array($arg)) {
					$out[] = '[ARRAY]';
				} else if (is_string($arg)) {
					if (strlen($arg) > 20) {
						$arg = ZUtils::Truncate($arg, 20);
					}
					$out[] = '"' . $arg . '"';
				} else {
					$out[] = $arg;
				}
			}
			return implode(', ', $out);
		}
		
		public static function strtotimeOrZero($dateString) {
			$val = strtotime($dateString);
			if (empty($val)) {
				return 0;
			}
			return $val;
		}
	
		public static function toCamelCase($str) {
			$str = preg_replace('/[^a-zA-Z0-9_ ]/', '', $str);
			$parts = preg_split("/[\ _\-]+/", ucwords($str));
			return lcfirst(implode('', $parts));
		}
	
		public static function CanWriteFile($path) {
			if (file_exists($path)) {
				return is_writable($path);
			} else {
				return is_writable(substr($path,0, strrpos($path, '/') ) );
			}
		}
	
		public static function Truncate($text, $length=60) {
			if (strlen($text) > $length)
				$text = substr($text, 0, $length - 3).'...';
			return $text;
		}
	
		public static function nn($val) {
			if ($val == null) {
				return '';
			} else {
				return $val;
			}
		}
	
		public static function h($str) {  // General escape function
			return htmlentities($str);
		}
	
		public static function orIfEmpty($val1, $val2) {
			if (!empty($val1)) {
				return $val1;
			}
			return $val2;
		}
	
	
		public static function pr($out, $label=false) {
			if (ZRequest::ShouldRenderAsHTML()) {
				if (!empty($label)) {
					echo '<div style="background: black;color:white;padding:2px;margin:4px 4px 0 4px">'.$label.'</div>';
				}
				echo '<pre style="border:1px solid black;padding:4px;margin:0 4px 4px 4px">'.print_r($out, true) . '</pre>';
			} else {
				if (!empty($label)) {
					echo '// '.$label.": \n";
				}
				echo print_r($out, true) . " \n\n";
			}
		}
	
		public static function StripHTML($text, $mode='all') {
			if (is_array($text)) {
				foreach ($text as $key => $value) {
					$text[$key] = ZUtils::StripHTML($value, $mode);
				}
				return $text;
			}
			// $text = str_ireplace(array('<br>','<br/>','<br />'),"\r", $text);
			if ($mode == 'all') {
				return strip_tags($text);
			} else if ($mode == 'format') {
				return strip_tags($text, '<p><b><em><strong><i><ul><ol><li>');
			} else if ($mode == 'mail') {
				if (strpos($text, '<html>') === false && strpos($text, '<div>') === false)
					return nl2br(strip_tags($text));
				else
					if (strpos($text, '<body') !== false) {
						$text = substr($text, strpos($text, '<body'));
					}
					$text = str_ireplace('<br>', '<br />', $text);
					$text = strip_tags($text, '<p><b><br><em><strong><i><ul><ol><li><div><span><blockquote>');
					$text = preg_replace('/\<p[^\>]*\>/', '<p>', $text);
					$text = preg_replace('/\<div[^\>]*\>/', '<div>', $text);
					$text = preg_replace('/\<blockquote[^\>]*\>/', '<blockquote>', $text);
					$text = preg_replace('/\<span[^\>]*bold[^\>]*\>/', '<boldspan>', $text);
					$text = preg_replace('/\<span[^\>]*\>/', '<span>', $text);
					$text = preg_replace('/\<boldspan\>/', '<span style="font-weight:bold;">', $text);
					return $text;
			} else {
				return strip_tags($text, $mode);
	
			}
		}
	
		public static function GetHashCode($theText, $iterations=1) {
			if (is_int($iterations)) {
				if ($iterations > 1) {
					$theText = ZUtils::GetHashCode($theText, $iterations - 1);
				}
				return md5('ctb1' . $theText);
			} else {
				return md5('ctb1' . $iterations . $theText);
			}
		}
	
		public static function PostXml($url, $xml) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/xml; charset=utf-8"));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
			$result = curl_exec($ch);
			curl_close($ch);
			return $result;
		}
	
		public static function PostCurl($url, $fields = array(), $options = array()) {
			$options = array_merge(array('dataType'=>'string'), $options);
	
			$files = array();
			if (!empty($options['files'])) {
				foreach ($options['files'] as $name => $f){
				   $files[$name] = file_get_contents($f);
				}
			}
	
			$cookies = self::$curlCookies;
			if (!empty($options['cookies'])) {
				$cookies = $options['cookies'];
			}
	
			if (!empty($fields)) {
				foreach ($fields as $key => $value){
					if (is_array($value)) {
						unset($fields[$key]);
						foreach ($value as $i => $str) {
							$fields[$key.'['.$i.']'] = $str;
						}
					}
				}
			}
	
			$curl = curl_init();
			$url_data = http_build_query($fields);
			$boundary = uniqid();
			$delimiter = '-------------' . $boundary;
			$post_data = ZUtils::BuildDataFilesForCurl($boundary, $fields, $files);
	
			$headerArray = array(
					"Content-Type: multipart/form-data; boundary=" . $delimiter,
					"Content-Length: " . strlen($post_data)
				);
	
			if (!empty($cookies)) {
				$c = array();
				foreach ($cookies as $key => $value) {
					$c[] = $key.'='.$value;
				}
				$headerArray[] = 'Cookie: '.implode(';', $c);
			}
	
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => $post_data,
				CURLOPT_HTTPHEADER => $headerArray,
			));
	
			if ($options['headers'] || $options['get_cookies']) {
				curl_setopt($curl, CURLOPT_HEADER, 1);
			}
	
			$response = curl_exec($curl);
			$info = curl_getinfo($curl);
			$err = curl_error($curl);
			$response = '' . mb_convert_encoding($response, 'ASCII');
	
			curl_close($curl);
			if ($options['dataType'] == 'json') {
	
				$result = json_decode($response, true);
				if ($result === null) {
					self::pr(htmlspecialchars($response), 'JSON could not be parsed');
					var_dump($response);
					self::pr(json_last_error_msg());
					self::pr(self::GetStackTrace());
					//throw new Exception('JSON could not be parsed: "'.$response.'"');
				}
				$response = $result;
			}
			if ($options['full']) {
				preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $response, $matches);
				$cookiesFromRequest = array();
				foreach($matches[1] as $item) {
				    parse_str($item, $cookie);
				    $cookiesFromRequest = array_merge($cookiesFromRequest, $cookie);
				}
				return array('response' => $response, 'info' => $info, 'error' => $err, 'cookies' => $cookiesFromRequest);
			}
			return $response;
		}
	
	
		public static function BuildDataFilesForCurl($boundary, $fields, $files){
		    $data = '';
		    $eol = "\r\n";
		    $delimiter = '-------------' . $boundary;
	
		    foreach ($fields as $name => $content) {
		        $data .= "--" . $delimiter . $eol
		            . 'Content-Disposition: form-data; name="' . $name . "\"".$eol.$eol
		            . $content . $eol;
		    }
	
		    foreach ($files as $name => $content) {
		        $data .= "--" . $delimiter . $eol
		            . 'Content-Disposition: form-data; name="' . $name . '"; filename="' . $name . '"' . $eol
		            //. 'Content-Type: image/png'.$eol
		            . 'Content-Transfer-Encoding: binary'.$eol
		            ;
	
		        $data .= $eol;
		        $data .= $content . $eol;
		    }
		    $data .= "--" . $delimiter . "--".$eol;
		    return $data;
		}
	
		public static function hoursToString($hours) {
			$out = [];
			$days = floor($hours / 24);
			$minutes = floor(($hours * 60) % 60);
			$hours = floor($hours) - ($days * 24);
			if ($days) {
				$out[] = $days . ' day' . ($days == 1 ? '' : 's');
			}
			if ($hours) {
				$out[] = $hours . ' hour' . ($hours == 1 ? '' : 's');
			}
			if ($minutes) {
				$out[] = $minutes . ' minute' . ($minutes == 1 ? '' : 's');
			}
			return implode(', ', $out);
		}

		public static function StartOfDay($timestamp) {
			return strtotime('midnight today', $timestamp);
		}
		
		public static function tsToDate($ts) {
			$date = date('r', $ts);
			return $date;
		}

		public static function LinkMailIfLoggedIn($email, $name) {
			if (empty(ZAuth::$loggedInUser)) {
				return $name;
			}
			return '<a href="mailto:' . ZUtils::StripHTML($email) . '">' . h($name) . '</a>';
		}
	
		public static function QuickTable($rows, $options = array()) {
			$firstRow = array_values($rows)[0];
			$keys = array_keys($firstRow);
			echo '<table><tr>';
			foreach ($keys as $key) {
				echo '<th>'.$key.'</th>';
			}
			echo '</tr>';
			foreach ($rows as $row) {
				echo '<tr>';
				foreach ($keys as $key) {
					$value = $html = $row[$key];
					if ($options[$key]) {
						if (!empty($options[$key]['joinRecord'])) {
							$html = $options[$key]['joinRecord'][$value]['name'];
						}
						if ($options[$key]['linkFragment']) {
							$html = '<a href="'.$options[$key]['linkFragment'].$value.'">'.$html.'</a>';
						}
					}
					echo '<td>'.$html.'</td>';
				}
				echo '</tr>';
			}
			echo '</table>';
		}

		public static function FloatsEqual($float1, $float2) {
			if (abs($float1 - $float2) < 0.0001) {
				return true;
			}
			return false;
		}
		
		public static function PrintReCaptchaV2() {
			?><div class="g-recaptcha" data-sitekey="<?=self::$reCaptchaPublicKey; ?>"><?php
		}
		
		public static function VerifyReCaptchaV2() {
			$reCaptchaPost = array(
				'secret' => self::$reCaptchaSecretKey,
				'response' => $_POST['g-recaptcha-response'],
				'remoteip' => $_SERVER['REMOTE_ADDR']
			);
			$reCaptchaResponse = self::PostCurl(self::$reCaptchaURL, $reCaptchaPost, array('dataType' => 'json'));
			return $reCaptchaResponse['success'] === 'true' || $reCaptchaResponse['success'] === true;
		}
	
	}
	
	


	function nn($val) {
		return ZUtils::nn($val);
	}

	function h($str) {
		return ZUtils::h($str);
	}

	function pr($out, $label=false) {
		return ZUtils::pr($out, $label);
	}


