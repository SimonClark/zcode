<?php
	define('DATE_FORMAT', $dateFormat = 'M d, Y');
	define('DATETIME_FORMAT', $dateTimeFormat = "M d, Y - g:ia");

	define('DRAFT_STATUS', -10);
	define('DELETED_STATUS', -1);
	define('NORMAL_STATUS', 0);
	define('SUSPENDED_STATUS', 1);
	define('REPORTED_STATUS', 2);

	define('FLOAT', 'float');
	define('INT', 'int');
	define('ALPHANUM', 'alphanum');
	define('ANY', 'any');


	define('HTTP_OK', 200);
	define('HTTP_MOVED_PERMANENTLY', 301);
	define('HTTP_SEE_OTHER', 303);
	define('HTTP_RESERVED', 306);
	define('HTTP_TEMPORARY_REDIRECT', 307);
	define('HTTP_PERMANENTLY_REDIRECT', 308);
	define('HTTP_BAD_REQUEST', 400);
	define('HTTP_UNAUTHORIZED', 401);
	define('HTTP_FORBIDDEN', 403);
	define('HTTP_NOT_FOUND', 404);
