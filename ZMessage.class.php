<?php
	class ZMessage {
		public static $mailHandler;
		
		public static function Init() {
			ini_set('sendmail_from', 'info@' . (ZCode::$mailDomain ? ZCode::$mailDomain : ZCode::$domain));
		}
		
		public static function SetMailHandler($mh) {
			self::$mailHandler = $mh;
		}		
		
		public static function SendMail($title, $message, $address, $from_mail='info') {
			$sep = "\r\n";
			if (strpos($from_mail, '@') === false) {
				$from_mail .= '@' . (ZCode::$mailDomain ? ZCode::$mailDomain : ZCode::$domain);
			}
			$from_name = ZCode::$mailFromName ? ZCode::$mailFromName : $from_mail;

			if (function_exists('ZMessage_SendMail')) {
				$success = ZMessage_SendMail($title, $message, $address, $from_mail, $from_name);
			} else if (self::$mailHandler) {
				$success = self::$mailHandler->SendMail($title, $message, $address, $from_mail, $from_name);
			} else {
				$encoding = "utf-8";

				// Preferences for Subject field
				$subject_preferences = array(
					"input-charset" => $encoding,
					"output-charset" => $encoding,
					"line-length" => 76,
					"line-break-chars" => $sep
				);
			
				// Mail header
				$header = "Content-type: text/html; charset=".$encoding." ".$sep;
				$header .= "From: ".$from_name." <".$from_mail."> ".$sep;
				$header = 'Reply-To: ' . $from . ' ' . $sep;
				$header .= 'Sender: ' . $from . ' ' . $sep;
				$header .= "MIME-Version: 1.0 ".$sep;
				$header .= "Content-Transfer-Encoding: 8bit ".$sep;
				$header .= "Date: ".date("r (T)")." ".$sep;
				$header .= iconv_mime_encode("Subject", $title, $subject_preferences);
						
				
				$success = mail($address, $title, wordwrap($message, 70), $header);
			}

			if (!$success) {
				ZLog::Log('Email Failed:', array('address' => $address, 'title' => $title, 'message' => $message, 'headers' => $header));
				ZRequest::Error('SendMail failed');
			} else {
				ZLog::Log('Email Sent:', array('address' => $address, 'title' => $title, 'message' => $message, 'headers' => $header));
			}
			if (function_exists('ZMessage_EmailSent')) {
				ZMessage_EmailSent($address, $title, $message, $header);
			}
			return $success;
		}

	}