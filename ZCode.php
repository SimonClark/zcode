<?php
	require_once(__DIR__.'/ZConstants.php');
	require_once(__DIR__.'/ZBase.class.php');
	require_once(__DIR__.'/ZUtils.class.php');
	require_once(__DIR__.'/ZDB.class.php');
	require_once(__DIR__.'/ZLog.class.php');
	require_once(__DIR__.'/ZRequest.class.php');
	require_once(__DIR__.'/ZForm.class.php');
	require_once(__DIR__.'/ZGD.class.php');
	require_once(__DIR__.'/ZAuth.class.php');
	require_once(__DIR__.'/ZMessage.class.php');
	require_once(__DIR__.'/ZI18n.class.php');
	require_once(__DIR__.'/ZValidate.class.php');
	
	class ZCode extends ZBase {

		public static $beta = false;
		public static $debug = false;
		public static $sessionName = 'site';
		public static $path = '/var/www/site.com/html/';
		public static $domain = 'site.com';
		public static $mailDomain = null;
		public static $siteName = 'site.com';
		public static $root = 'http://site.com/';
		public static $zcodePath = '/includes/zcode/';
		public static $mailFromName = null;

		public static function Init() {
			date_default_timezone_set('America/Toronto');
			
			global $skipSession;
			
			self::Config('site');
			
			if (!$skipSession) {
				session_start();
			}

			if (self::$beta) {
				error_reporting(E_ALL ^ E_NOTICE);
			} else {
				error_reporting(E_ALL | E_STRICT);
			}

			ZDB::Init();
			ZAuth::Init();
			ZRequest::Init();
			ZI18n::Init();
			ZMessage::Init();
			ZUtils::Init();
		}

		public static function GetZCodeForJS() {
			return array(
				'path' => ZCode::$zcodePath,
				'lang' => ZI18n::GetLang(),
				'i18n' => ZI18n::GetJSTranslations(),
				'auth' => (ZAuth::$loggedInUser ? 1 : 0)
			);
		}
		
	}
	
	ZCode::Init();