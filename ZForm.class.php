<?php

	class ZForm {

		public static function GetSelectTag($name, $options, $value, $params = array()) {
			$attributes = self::GetAttributesString($params);
			$out = '<select name="'.$name.'" id="'.$name.'" '.$attributes.' >';
			foreach ($options as $key => $optName) {
				$out .= '<option value="'.trim($key).'"';
				if ($key == $value) $out .= ' SELECTED="SELECTED"';
				$out .= '>'.$optName.'</option>';
			}
			$out .= '</select>';
			return $out;
		}

	
		public static function GetRichSelectTag($name, $options, $value, $params = array()) {
			$out = '<select name="'.$name.'" class="rich-menu" ' . self::GetAttributesString($params) . ' >';
			$out .= self::GetRichSelectOptions($options, $value);
			$out .= '</select>';
			return $out;
		}
	
		public static function GetRichSelectOptions($options, $value) {
			if (empty($options)) {
				return '';	
			}
			$out = '';
			foreach ($options as $key => $option) {
				if ($option['type'] == 'opt') {
					$out .= self::GetRichSelectOption($key, $option, $value);
					$out .= self::GetRichSelectOptions($option['items'], $value);
				} else {
					$out .= '<optgroup label="'.$option['name'].'">';
					$out .= self::GetRichSelectOptions($option['items'], $value);
					$out .= '</optgroup>';
				}
			}
			return $out;
		}
		
		public static function GetRichSelectOption($key, $option, $value) {
			$out = '<option value="'.$key.'" ';
			if ($key == $value) {
				$out .= ' SELECTED="SELECTED"';
			}
			$attributes = self::GetAttributesString($option);
			$out .= $attributes . ' >'.$option['name'].'</option>';
			return $out;
		}

		public static function GetTextTag($type, $name, $value, $params=array()) {
			$attributes = self::GetAttributesString($params);
			return '<input type="'.$type.'" name="'.$name.'" value="' . $value . '" '.$attributes.' />';
		}
	
		public static function GetCheckboxTag($name, $value, $params = array()) {
			$out = '<input type="checkbox" name="'.$name.'" value="1" ' . self::GetAttributesString($params) . ' ';
			if (!empty($value)) {
				$out .= 'checked="checked"';
			}
			$out .= " />";
			return $out;
		}


		public static function GetAttributesString($params) {
			$attributes = '';
			if (!empty($params['attr'])) {
				foreach ($params['attr'] as $attrName => $attrValue) {
					$attributes .= $attrName . '="'.$attrValue.'" ';
				}
			}
			return $attributes;
		}
		
	}

