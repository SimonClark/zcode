<?php
	class ZBase {

		public static function Config($params) {
			if (is_string($params)) {
				global $ZCodeConf;
				$params = $ZCodeConf[$params];
			}
			
			if (!empty($params)) {
				foreach ($params as $name => $value) {
					static::$$name = $value;
				}
			}
		}
		
	}