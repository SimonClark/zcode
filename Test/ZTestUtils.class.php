<?php
	class ZTestUtils {
		const NAMES = array('James','Mary','John','Patricia','Robert','Jennifer','Michael','Linda','William','Elizabeth','David','Barbara','Richard','Susan','Joseph','Jessica','Thomas','Sarah','Charles','Karen','Christopher','Nancy','Daniel','Margaret','Matthew','Lisa','Anthony','Betty','Donald','Dorothy','Mark','Sandra','Paul','Ashley','Steven','Kimberly','Andrew','Donna','Kenneth','Emily','Joshua','Michelle','George','Carol','Kevin','Amanda','Brian','Melissa','Edward','Deborah','Ronald','Stephanie','Timothy','Rebecca','Jason','Laura','Jeffrey','Sharon','Ryan','Cynthia','Jacob','Kathleen','Gary','Helen','Nicholas','Amy','Eric','Shirley','Stephen','Angela');
		/*
		function doPost($url, $postParams, $dataType = 'string') {
			$options = array(
			    'http' => array(
			        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
			        'method'  => 'POST',
			        'content' => http_build_query($postParams)
			    )
			);
			$context = stream_context_create($options);
			$result = file_get_contents($url, false, $context);
			if ($result === FALSE) { 
				echo 'Error in post';
			}
			if ($dataType == 'json') {
				$result = json_decode($result, true);
			}
			
			return $result;
		}
		*/
		
		public static function Message($message, $value, $class='') {
			echo '<div class="message '.$class.'">'.$message.': <span>'.$value.'</span></div>';
		}	
			
		public static function RandomName() {
			return ZTestUtils::NAMES[rand(0,count(ZTestUtils::NAMES)-1)];
		}
	
	
		public static function LogFailure($message, $expected = ZAssert::NO_INPUT, $actual = ZAssert::NO_INPUT) {
			global $responseData;
			$stackTrace = ZUtils::GetStackTrace();
			echo '<div class="failure">Failed test: '.$message . '<span class="src">'.$stackTrace[2].'</span>';
	
			if ($expected != ZAssert::NO_INPUT) {
				pr($expected, 'Expected');
			}
			if ($actual != ZAssert::NO_INPUT) {
				pr($actual, 'Actual');
			}
			echo '<div class="response-log"><button class="show">Show Response Data...</button><div class="response"><pre>';
			echo htmlentities(print_r($responseData, true));
			echo '</pre></div></div>';
			echo '</div>';
		}
		
		public static function RandomHexString($len) {
			$out = '';
			while ($len > 0) {
				$out .= dechex(rand(0,15));
				$len--;
			}
			return $out;
		}
		
		
	}
