<?php
	class ZAssert {
		public static $testsPassed = 0;
		public static $testsFailed = 0;
		public static $testsFailedSinceLastCheck = 0;
		const NO_INPUT = 43428765;

		public static function TestPassed() {
			self::$testsPassed++;
		}

		public static function TestFailed($msg, $expected = self::NO_INPUT, $actual = self::NO_INPUT) {
			ZTestUtils::LogFailure($msg, $expected, $actual);
			self::$testsFailed++;
			self::$testsFailedSinceLastCheck++;
		}

		public static function Equals($msg, $expected, $actual) {
			$type = gettype($expected);
			if ($type == 'float' || $type == 'double') {
				$actual = floatval($actual);
				$equals = ZUtils::FloatsEqual($actual, $expected);
			} else {
				$equals = $expected == $actual;
			}
			if ($equals) {
				self::TestPassed();
			} else {
				self::TestFailed($msg, $expected, $actual);
			}
		}

		public static function NotEquals($msg, $expected, $actual) {
			$type = gettype($expected);
			if ($type == 'float' || $type == 'double') {
				$actual = floatval($actual);
				$equals = ZUtils::FloatsEqual($actual, $expected);
			} else {
				$equals = $expected == $actual;
			}
			if (!$equals) {
				self::TestPassed();
			} else {
				self::TestFailed($msg, $expected, $actual);
			}
		}

		public static function IsString($msg, $value) {
			$type = gettype($value);
			if ($type != 'string') {
				self::TestFailed($msg, 'string', $type);
			} else {
				self::TestPassed();
			}
		}

		public static function StringLengthIsLessThan($msg, $string, $expectedLength) {
			self::IsString($msg, $string);
			$actualLength = strlen($string);
			if ($actualLength > $expectedLength) {
				self::TestFailed($msg, 'string is longer than expected '.$expectedLength, $string);
			} else {
				self::TestPassed();
			}
		}

		public static function IsArray($msg, $value) {
			$type = gettype($value);
			if ($type != 'array') {
				self::TestFailed($msg, 'type is array', $value);
			} else {
				self::TestPassed();
			}
		}

		public static function IsType($msg, $expectedtype, $value) {
			$type = gettype($value);
			if ($type != $expectedtype) {
				self::TestFailed($msg, $expectedtype, $type);
			} else {
				self::TestPassed();
			}
		}

		public static function StringContains($msg, $string, $substring) {
			self::IsString($msg . ' - Assert is String', $string);
			if (strpos($string, $substring) === false) {
				self::TestFailed($msg, 'contains "'.$substring.'"', '<pre>'.htmlentities($string).'</pre>');
			} else {
				self::TestPassed();
			}
		}

		public static function StringDoesNotContain($msg, $string, $substring) {
			self::IsString($msg . ' - Assert is String', $string);
			if (strpos($string, $substring) !== false) {
				self::TestFailed($msg, 'does not contain "'.$substring.'"', '<pre>'.htmlentities($string).'</pre>');
			} else {
				self::TestPassed();
			}
		}

		public static function IsNotNull($msg, $value) {
			if ($value === null) {
				self::TestFailed($msg, 'NULL', '<pre>'.print_r($value).'</pre>');
			} else {
				self::TestPassed();
			}
		}

		public static function IsNull($msg, $value) {
			if ($value !== null) {
				self::TestFailed($msg, 'Something', 'null');
			} else {
				self::TestPassed();
			}
		}

		public static function IsTrue($msg, $value) {
			if ($value !== true) {
				self::TestFailed($msg, 'True', $value);
			} else {
				self::TestPassed();
			}
		}

		public static function IsFalse($msg, $value) {
			if ($value !== false) {
				self::TestFailed($msg, 'True', $value);
			} else {
				self::TestPassed();
			}
		}

		public static function IsEmpty($msg, $value) {
			if (!empty($value)) {
				self::TestFailed($msg, '<Nothing>', $value);
			} else {
				self::TestPassed();
			}
		}

		public static function IsNotEmpty($msg, $value) {
			if (empty($value)) {
				self::TestFailed($msg, '<Something>', $value);
			} else {
				self::TestPassed();
			}
		}

		public static function ArrayValuesMatch($msg, $expected, $valueArray) {
			foreach ($expected as $key => $value) {
				if (!array_key_exists($key, $valueArray)) {
					self::TestFailed($msg, 'Array key does not exist: ',$key);
				} else {
					self::Equals($msg.' - '.$key, $value, $valueArray[$key]);
				}
			}
		}

		public static function ArrayContainsKey($msg, $key, $valueArray) {
			self::IsArray($msg, $valueArray);
			if (!array_key_exists($key, $valueArray)) {
				self::TestFailed($msg . ' - Array key does not exist: '.$key, null, $valueArray);
			} else {
				self::TestPassed();
			}
		}

		public static function ArrayDoesNotContainKey($msg, $key, $valueArray) {
			self::IsArray($msg, $valueArray);
			if (array_key_exists($key, $valueArray)) {
				self::TestFailed($msg . ' - Array key exists: '.$key, null, $valueArray);
			} else {
				self::TestPassed();
			}
		}


	}

