<html>
	<head>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<style>
			.failure {
			    background-color: #fee;
			    padding: 4px;
			    border: 1px solid darkred;
			    border-radius: 4px;
			    margin: 2px;
			}
			
			.summary-box {
			    position: fixed;
			    top: 15px;
			    right: 15px;
			}

			.message {
				background-color: #e9f9f1;
				padding: 5px 12px;
				border: 2px solid rgba(40,40,40,0.9);
				border-radius: 4px;
				margin: 4px;
				font-size: 1.3em;
				display: inline-grid;
				text-align: center;
				box-shadow: 5px 5px 5px 0px rgba(0,0,0,0.2);
			}			
			.message span {
			   color: #000;
			   font-weight: bold;
			   font-size: 1.4em;
			}
			
			.message.bad {
				background-color: #f7d3d3;
			}
			.message.bad span {
			   color: #800;
			}
			.response-log .response {
				display: none;
			}

			div.failure span.src {
			    padding-left: 10px;
			    color: rgba(100,0,0,0.7);
			    font-size: 0.9em;
			}
		</style>
		<script>
			$(document).ready(function(){
				$('.response-log button.show').click(function(){
					$(this).parent().find('.response').show();
					$(this).hide();
				})
			});						
		</script>
	</head>
	<body>
		<div class="main test">
			<h2><?=ZCode::$siteName; ?> Tests</h2>


