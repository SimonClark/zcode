<?php
	class ZRequest extends ZBase {
		protected static $header = '_header.php';
		protected static $footer = '_footer.php';
		protected static $sourceIPs = array();
		protected static $serverIP = '';
		protected static $renderingHTML = false;
		protected static $renderingPlainText = false;
		public static $shouldRenderAsHTML = null;
		public static $headerSnippet = '';
		public static $bodyClass = '';
		public static $useJQuery = false;
		public static $useHighCharts = false;
		private static $pageTitle = null;
		private static $jqueryAdded = false;
		private static $headElements = array();

		public static function Init() {
			self::Config('request');	
		}
		
		public static function Header($pageTitle = false) {
			if ($pageTitle) {
				self::SetPageTitle($pageTitle);	
			}
			include(ZCode::$path.self::$header);
		}
		
		public static function Footer() {
			include(ZCode::$path.self::$footer);
		}
		
		public static function PrintHeadElements() {
			self::$useJQuery && self::PrintJQuery();
			self::$useHighCharts && self::PrintHighCharts();
			ZI18n::PrintI18nAuthoring();
			?><script>zcode = <?=json_encode(ZCode::GetZCodeForJS()); ?>;</script><?php
			echo implode("\r", self::$headElements);
		}
		
		public static function AddHeadElements($html) {
			self::$headElements[] = $html;
		}

		public static function PrintJQuery() {
			if (!self::$jqueryAdded) {
				?>
					<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
					<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
					<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>			
				<?php
				self::$jqueryAdded = true;
			}	
		}
		
		public static function PrintHighCharts() {
			?>
			<script src="https://code.highcharts.com/highcharts.js"></script>
			<script src="https://code.highcharts.com/modules/exporting.js"></script>
			<script src="https://code.highcharts.com/modules/export-data.js"></script>
			<?php	
		}
		
		public static function PrintHeaderSnippet() {
			echo self::$headerSnippet;
		}

		public static function LimitToSource() {
			if (array_search($_SERVER['REMOTE_ADDR'], self::$sourceIPs) === false) {
				die('Authentication Failed: SRD_Check');
			}
			if ($_SERVER['REQUEST_SCHEME'] != 'https') {
				die('Authentication Failed: SIHS_Check');
			}
		}

		public static function LimitToLocalhost() {
			global $beta;
			if (!($beta || $_SERVER['REMOTE_ADDR'] == '127.0.0.1' || $_SERVER['REMOTE_ADDR'] == self::$serverIP)) {
				die ('401 ' . $_SERVER['REMOTE_ADDR']);
			}

		}

		public static function LimitToBeta() {
			global $beta;
			if (!$beta) {
				die ('401 ' . $_SERVER['REMOTE_ADDR']);
			}
		}

		public static function EnforceHttps() {
			global $debug;
			if ($debug) {
				return;
			}
			if (strtolower($_SERVER['REQUEST_SCHEME']) != 'https') {
				$url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				header("Location: ".$url); die; return;
			}
		}
		
		public static function renderingHTML($isHTML) {
			self::$renderingHTML = $isHTML;
		}
		
		public static function renderingPlainText($isPlainText) {
			self::$renderingPlainText = $isPlainText;
		}
		
		public static function ShouldRenderAsHTML() {
			if (self::$renderingHTML) {
				return true;
			} else if (self::$renderingPlainText) {
				return false;				
			} else if (self::$shouldRenderAsHTML == null) {
				$path = $_SERVER['SCRIPT_NAME'];
				self::$shouldRenderAsHTML = (stripos($path, '/ajax/') === false) && (stripos($path, '/interface/') === false);
			}
			return self::$shouldRenderAsHTML;
		}

		public static function SetPageTitle($title) {
			self::$pageTitle = $title;
		}

		public static function GetPageTitle($tag = null, $class = null) {
			if (empty(self::$pageTitle)) {
				return '';
			}
			if (empty($tag)) {
				return self::$pageTitle;
			}
			return '<' . $tag . ' class="' . $class . '">' . self::$pageTitle . '</' . $tag . '>';
		}

		public static function Redirect($aPage, $aMessage="") {
			http_response_code(HTTP_SEE_OTHER);
			header("Location: " . $aPage);
			die($aMessage. '<br>'."Location: " . $aPage);
		}

		public static function Die($msg = "Illegal Operation") {
			self::ShouldRenderAsHTML() && self::Header();
			echo '<div class="warning">'.$msg.'</div>';
			self::ShouldRenderAsHTML() && self::Footer();
			die;
		}

		public static function ExitPage($message, $_pageName, $statusCode = 200) {
			global $pageName;
			$html = self::ShouldRenderAsHTML();
			$pageName = $_pageName;
			http_response_code($statusCode);
			self::ShouldRenderAsHTML() && self::Header();
			echo $message;
			if (self::ShouldRenderAsHTML()) {
				self::Footer();
			} else if ($statusCode != 200) { 
				ZLog::Print(); 
			}
			die;
		}

		public static function Error($message, $errors = array()) {
			if (function_exists('ZAuth_HandleError')) {
				ZAuth_HandleError($message, $errors);	
			}
			if (!empty($errors)) {
				$message .= '<pre>' . print_r($errors, true) . '</pre>';
			}
			$message .= '<pre>' . print_r(ZUtils::GetStackTrace(), true) . '</pre>';
			ZRequest::ExitPage($message, 'Error', HTTP_BAD_REQUEST);
		}
		
		public static function Forbidden($message) { ZRequest::ExitPage($message, 'Forbidden', 403); }
		public static function Notice($message) { ZRequest::ExitPage($message, 'Notice'); }

		public static function SetFlash($msg) {
			$_SESSION['flash_msg'] = $msg;
		}

		public static function GetFlash($class = false) {
			if (empty($_SESSION['flash_msg'])) {
				return false;
			}
			$msg = $_SESSION['flash_msg'];
			unset($_SESSION['flash_msg']);
			if ($class) {
				return '<div class="'.$class.'">'.$msg.'</div>';
			}
			return $msg;
		}

		public static function SetSessionVariable($key, $msg) {
			$_SESSION[$key] = $msg;
		}

		public static function GetSessionVariable($key) {
			if (empty($_SESSION[$key])) {
				return false;
			}
			$msg = $_SESSION[$key];
			unset($_SESSION[$key]);
			return $msg;
		}

		public static function DieIfError() {
			if (!empty(ZLog::$errors)) {
				ZRequest::Error('There were Request Errors', ZLog::$errors);
			}
		}

		public static function DieIfErrors() {
			ZRequest::DieIfError();
		}

		public static function OutputAsDownload($fileName, $content) {
			$size = mb_strlen($content);
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $fileName); 
			header('Content-Transfer-Encoding: binary');
			header('Connection: Keep-Alive');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			header('Pragma: public');
			header('Content-Length: ' . $size);
			echo $content;
			die;
		}

		public static function OutputIfTesting($id) {
			if (ZAuth::$automatedTests) {
				echo $id;
				die;
			}
		}
						
	}
	
	
	
	
	